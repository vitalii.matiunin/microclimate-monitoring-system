#include "MQ135.h"
#include "DHT.h"

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define DHTPIN 3
#define MQ_PIN A0 
#define SIGNAL_PIN 5

#define SCREEN_WIDTH 128 //128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)

MQ135 CO2_sensor = MQ135(MQ_PIN);
DHT dht(DHTPIN, DHT22);

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

float t, h, CO2, av_t=0, av_h=0, av_co2=0, alpha = 0.2, timer=0;
int counter=0, motion=0;

void setup() {
  Serial.begin(115200);
  pinMode(SIGNAL_PIN, INPUT);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
  Serial.println(F("SSD1306 allocation failed"));
  for(;;); 
  }

  dht.begin();
  
  display.clearDisplay();
  display.display();


                                      // TODO add calibrating
//  float calcR0 = 0;
//  for(int i = 1; i<=10; i ++)
//  {
//    MQ135.update();
//    calcR0 += MQ135.calibrate(RatioMQ135CleanAir);
//  }
// MQ135.setR0(calcR0/10);
}

void loop() {
  //read values
  motion = digitalRead(SIGNAL_PIN);
  h = dht.readHumidity();
  t = dht.readTemperature();
  CO2 = CO2_sensor.getCorrectedPPM(t,h);
  //averaging
  av_t = av_t + t;
  av_h = av_h + h;
  av_co2 = av_co2 + CO2;
  if(motion) {
    print_text(t, h, CO2);
    display.display();
  } else {display.clearDisplay(); }
  display.display();
    //CO2 = Exponentialsmoothing(av_co2,CO2,alpha);
 
  if(millis() - timer > 60000){
      t = av_t / counter;
      h = av_h / counter;
      CO2 = av_co2 / counter;
      Serial.print("c");
      Serial.println(CO2);
      Serial.print("h");
      Serial.println(h);
      Serial.print("t");
      Serial.println(t);
      counter = 0;
      av_t = 0; av_h = 0; av_co2 = 0;
      timer = millis();
  } 
  counter++;
}

float Exponentialsmoothing(float s, float z, float alpha){
  float result;
  result = s + alpha*(z-s);
  return result;
}

void print_text(float t, float h, float c) {
  display.clearDisplay();
  
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  
  display.setCursor(0, 0);
  char temp_buf[30];
  String temp_val = String(t);
  temp_val.remove(temp_val.length() - 1, 2);
  String temp = "t: "+ temp_val +"C";
  temp.toCharArray(temp_buf, 22);
  display.println(temp_buf);
  
  display.setCursor(0, 22);
  char hum_buf[30];
  String hum_val = String(h);
  hum_val.remove(hum_val.length() - 1, 1);
  String hum = "h: " + hum_val + "%";
  hum.toCharArray(hum_buf, 22);
  display.println(hum_buf);

  display.setCursor(0, 44);
  char buf[50];
  String val = String(c);
  val.remove(val.length() - 1, 1);
  String s = "co2: " + val + "ppm";
  s.toCharArray(buf, 22);
  display.println(buf); 
}
