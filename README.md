# Microclimate Monitoring System #
## Project Discription ##
The mission of the project is to provide data for customers which helps to control microclimate inside of the apartment and avoid critical air conditions which are unsafe for human.

Our monitoring system measures the main microclimate parameters such as air temperature, humidity and CO2 concentrartion. In case of parameters limits exceedings, the user will get a notification via Telegram-bot with recommendation about microclimate improvement. Also, the user can see the statistics over the observation period on the web-site and current parameters on the embedded screen. 

General view of the Microclimate Monitoring System:

_(place for the image)_

Notification Example:

![](Images/Notification_Example.png)

Example of [Web-page with statistics](https://console.thinger.io/#!/dashboards/MicroclimateDashboard?authorization=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJEYXNoYm9hcmRfTWljcm9jbGltYXRlRGFzaGJvYXJkIiwidXNyIjoiUXVldGFsYXMifQ.-qHmjtVxYKkenNH5psiKQQX4IX-OVINFo8-4q_k0VhQ):

![](Images/Web-site_image.png)

The Microclimate Monitoring System Concept of Operation visually represented as follow:

![](Images/Concept_of_operations_image.png)

***
## Related Documents ##
Below you can find documents which were presented during the product lifecycle:
+ [Stakeholder analysis](https://miro.com/app/board/o9J_lffHwGw=/)
+ [Preliminary Design Review Presentation](https://drive.google.com/file/d/1hW7wOU0w5dTES5n3qFjR_d4BGq2rzqY6/view?usp=sharing)
+ [Critical Design Review Presentation](https://drive.google.com/file/d/1hGAQccobYFZY8NRrUmZwyIu2CXUUJFDI/view?usp=sharing)
+ [Final Presentation](https://prezi.com/p/edit/qc5cwhwd3orc/)

***
## Measurement Device Description ##

The Measurement Device consists of 2 senors: MQ-135 (CO2 concentration measuring) and DHT-22 (temperature and humitidy measuring). Embedded SunFounder 0.96 inches OLED display is equipped with HC-SR501 Motion Sensor in order to decrease power consumption. 9V AC-DC Adapter is needed for Power Supply Network connecting. Arduino Uno microcontroller performs general system control, ESP8266 WiFi Module enables the data transmission to the Internet. System's Structure consists of wires, breadboard and plastic case. 

Electrical Scheme of Microclimate Monitoring System Measurement Device is represented below:

![](Images/Electrical_Scheme_Image.png)

List of the used electronic equipment (links follow to datasheets):
+ [Arduino Uno](https://store.arduino.cc/usa/arduino-uno-rev3)
+ [ESP8266 WiFi Module](https://www.electroschematics.com/esp8266-datasheet/)
+ [Temperature and Humidity Sensor DHT22](https://www.sparkfun.com/datasheets/Sensors/Temperature/DHT22.pdf)
+ [CO2 Sensor MQ-135](https://www.electronicoscaldas.com/datasheet/MQ-135_Hanwei.pdf)
+ [SunFounder 0.96 inches OLED display](https://www.vishay.com/docs/37902/oled128o064dbpp3n00000.pdf)
+ [Motion Sensor HC-SR501](https://components101.com/hc-sr501-pir-sensor)

***
## References ##
+ Wifi module ESP8266
> 1. [Connection scheme]( https://www.instructables.com/Arduino-UNO-ESP8266-WiFi-Module/)
> + How to upload code to ESP8266
>> 1. Read reference 1 above
>> 2. Conncet RX to RX, TX to TX for uploding.
>> 3. Connect GPIO 0 to GND
>> 4. Press the reset buttom on Arduino while flashing ()
>> 5. Change connection RX-TX TX-RX for operating.
>> 6. GPIO 0 to VCC.
+ CO2 sensor MQ135
> 1. [Library](https://hackaday.io/project/3475-sniffing-trinket/log/12363-mq135-arduino-library)
> 2. [Calibration](https://mysku.ru/blog/aliexpress/41409.html)
