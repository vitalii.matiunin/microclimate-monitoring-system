#include <ThingerESP8266.h>

#define USERNAME "Quetalas"
#define DEVICE_ID "ArduinoEsp8266"
#define DEVICE_CREDENTIAL "y0tzRz@D5Q@r"

#define SSID "MERCUSYS_4"
#define SSID_PASSWORD "QqQqQqQq1secure1"

ThingerESP8266 thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL);
pson WarningCo2;

float t=0, h=0, co2=0;
String data;

void setup() {
  Serial.begin(115200);
  thing.add_wifi(SSID, SSID_PASSWORD);

  // resource output example (i.e. reading a sensor value)
  thing["millisec"] >> outputValue(millis());
  thing["temp"] >> outputValue(t);
  thing["hum"] >> outputValue(h);
  thing["co2"] >> outputValue(co2);

  // more details at http://docs.thinger.io/arduino/
}

void loop() {
  data = Serial.readStringUntil('\n');
  if (data[0] == 't')
  {
    data.remove(0,1);
    t = data.toFloat();
    }
  if (data[0] == 'h')
  {
    data.remove(0,1);
    h = data.toFloat();
    }
    if (data[0] == 'c')
  {
    data.remove(0,1);
    co2 = data.toFloat();
    if (co2 > 2000){
      WarningCo2["CO2"] = co2;
      thing.call_endpoint("CO2Warning", WarningCo2);
      }
    }
  thing.handle();
}
